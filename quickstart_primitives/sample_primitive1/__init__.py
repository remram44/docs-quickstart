from .input_to_output import InputToOutputPrimitive


__all__ = ['InputToOutputPrimitive']

from pkgutil import extend_path
__path__ = extend_path(__path__, __name__)  # type: ignore
