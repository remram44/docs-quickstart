# D3M-TA1 primitive project

A general TA1 project repo for building TA1 primitives or testing `D3M`.

**Note:** This is the working approximate structure.
In order to start building primitives, change the directory/filenames accordingly.

Refer to: https://docs.datadrivendiscovery.org/devel/quickstart.html for a quick start tutorial.

Supported `D3M` version -`V2020.1.9`

### Structure of repository
The directory and file structure is defined as follows:

```
<docs-quickstart>/
  <quickstart_primitives>/
    <config_files>
       config.py
    <sample_primitive1>/
        __init__.py
        <primitive_name>.py
        <primitive_name_test>.py
      .
      .
      .
    <sample_primitive10>/
          <primitive_name>.py
    output/
          <....>
    generate-primitive-json.py
  pipelines/
  setup.py
```

### Running the code

**Step-1:** Install the package and its dependencies
```
./run-docker.sh

pip3 install -e .
```

**Step-2:** Generate primitive `json`
```
cd quickstart_primitives

python3 generate-primitive-json.py

exit
```

**Step-3:** Generate Pipeline
```
./run_pipeline_<select>.sh
```

### Code References

1. [USC-ISI TA1 primitives](https://github.com/usc-isi-i2/dsbox-primitives)
2. [Auton Lab TA1 primitives](https://github.com/autonlab/autonbox)
3. [D3M Common TA1 primitives](https://gitlab.com/datadrivendiscovery/common-primitives)
