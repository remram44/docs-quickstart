from setuptools import setup, find_packages

setup(
    name='docs_quickstart',
    version='0.1.0',
    description='Quickstart primitives',
    author='My Name',
    url='https://gitlab.com/datadrivendiscovery/docs-quickstart',
    packages=find_packages(exclude=['pipelines']),
    keywords='d3m_primitive',
    install_requires=[
        'd3m',
    ],
    entry_points={
        'd3m.primitives': [
            'operator.input_to_output.Quickstart = quickstart_primitives.sample_primitive1:InputToOutputPrimitive',
            'feature_extraction.cnn.UBC = quickstart_primitives.sample_primitive2:CNNFeatureExtract',
        ],
    },
    license='Apache-2.0',
    zip_safe=False,
    python_requires='>=3.6',
)
